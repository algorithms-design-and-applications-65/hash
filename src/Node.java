import java.lang.Math;

public class Node {
    int key;
    String value;
    private String[] ar = new String[100];

    private int hash(int key) {
        return key ;
    }

    public String get(int key) {
        return ar[hash(key)];
    }

    public void put(int key, String value) {
        ar[hash(key)] = value;
    }

    public void remove(int key) {
        ar[hash(key)] = null;
    }

}
